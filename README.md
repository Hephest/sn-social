# StarNavi Social

![sn-social](https://i.ibb.co/0MjJ9BY/Screenshot-2019-10-07-Star-Navi-Social-API-1.png)

Test task for StarNavi company

## Getting Started

TODO

### Prerequisites

TODO

### Installing

TODO

## TODO List

* [x] ~~JWT Auth~~
* [x] ~~Class-based routes~~
* [x] ~~(Optional) Swagger UI~~
* [ ] Permissions
* [ ] Tests

## Running the tests

TODO

## Built With

* [Django](https://docs.djangoproject.com/en/2.2/) - The web framework used
* [Django REST Framework](https://www.django-rest-framework.org/) -  Powerful and flexible toolkit for building Web APIs
* [drf-yasg](https://github.com/axnsan12/drf-yasg) - Generate real Swagger/OpenAPI 2.0 specifications from a Django Rest Framework API
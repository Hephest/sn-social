from rest_framework import serializers

from core.models import Post


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ['url', 'author', 'title', 'text', 'created_date', 'published_date']

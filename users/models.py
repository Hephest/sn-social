from django.contrib.auth.models import User
from django.db import models


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    dob = models.DateField()
    about = models.TextField()
    country = models.CharField(max_length=50)

    def __str__(self):
        return "{} : {} {}".format(self.user.id, self.user.first_name, self.user.last_name)